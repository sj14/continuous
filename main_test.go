package main

import "testing"

func TestDoubleN(t *testing.T) {
	want := 4
	got := DoubleN(2)

	if got != want {
		t.Error("wanted: %v, got: %v", want, got)
	}
}
